<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FacebookLoginController;

Route::prefix("admin")->group(function(){
    Route::name("admin.")->group(function(){
        require_once 'admin-routes.php';
    });
});

Route::prefix("facebook")->group(function(){
   Route::get("login",[FacebookLoginController::class,"index"]);
   Route::get("setup",[FacebookLoginController::class,"setup"]);
   Route::get("callback",[FacebookLoginController::class,"callback"])->name("fb.callback");
   Route::get("delete",[FacebookLoginController::class,"delete"])->name("fb.delete");
});