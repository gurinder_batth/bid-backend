<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\DriverController;
use App\Http\Controllers\Admin\ProductController;

Route::get("dashboard",[HomeController::class,"dashboard"])->name("dashboard");

Route::prefix("product")->group(function(){
    Route::name("product.")->group(function(){
       Route::get("add",[ProductController::class,"add"])->name("add");
       Route::post("create",[ProductController::class,"create"])->name("create");
       Route::get("list",[ProductController::class,"list"])->name("list");
       Route::get("delete/{id}/{flag}",[ProductController::class,"delete"])->name("delete");
       Route::get("edit/{id}",[ProductController::class,"edit"])->name("edit");
       Route::post("update",[ProductController::class,"update"])->name("update");
    });
});

Route::prefix("category")->group(function(){
    Route::name("menu.")->group(function(){
        Route::get("add",[MenuController::class,"add"])->name("add");
        Route::post("create",[MenuController::class,"create"])->name("create");
        Route::get("list",[MenuController::class,"list"])->name("list");
        Route::get("delete/{id}/{flag}",[MenuController::class,"delete"])->name("delete");
        Route::get("edit/{id}",[MenuController::class,"edit"])->name("edit");
        Route::post("update",[MenuController::class,"update"])->name("update");
     });
});

Route::prefix("user")->group(function(){
    Route::name("user.")->group(function(){
        Route::get("list",[UserController::class,"list"])->name("list");
     });
});

Route::prefix("order")->group(function(){
    Route::name("order.")->group(function(){
        Route::get("list",[OrderController::class,"list"])->name("list");
        Route::get("single/{id}",[OrderController::class,"single"])->name("single");
        Route::post("accept",[OrderController::class,"accept"])->name("accept");
        Route::get("inovice/{id}",[OrderController::class,"inovice"])->name("inovice");
     });
});

Route::prefix("driver")->group(function(){
    Route::name("driver.")->group(function(){
        Route::get("list",[DriverController::class,"list"])->name("list");
        Route::get("add",[DriverController::class,"add"])->name("add");
        Route::post("create",[DriverController::class,"create"])->name("create");
        Route::get("password/{id}",[DriverController::class,"password"])->name("password");
     });
});


