<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Bid @yield('title')
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />
    <!--     Fonts and icons     -->

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{URL('public/new-admin-master/assets/css/material-dashboard.css')}}" rel="stylesheet" />
   
     @yield('css')
     <style>


     label{
        text-transform: uppercase;
        font-weight: bold;
        text-align: center;
     }
    
    .btn-pv{
        background: #77c323 !important;
        border: solid 2px black  !important;
        color: black  !important;
        font-weight: bold  !important;
        letter-spacing: 2px  !important;
        font-size: 14px  !important;
    }
       


input[type=checkbox] + label {
  display: inline;
  margin: 0.2em;
  cursor: pointer;
  padding: 0.2em;
}

input[type=checkbox] {
  display: none;
}

input[type=checkbox] + label:before {
  content: "\2714";
  border: 0.1em solid #000;
  border-radius: 0.2em;
  display: inline-block;
  width: 1em;
  height: 1em;
  padding-left: 0.2em;
  padding-bottom: 0.3em;
  margin-right: 0.2em;
  vertical-align: bottom;
  color: transparent;
  transition: .2s;
}

input[type=checkbox] + label:active:before {
  transform: scale(0);
}

input[type=checkbox]:checked + label:before {
  background-color: MediumSeaGreen;
  border-color: MediumSeaGreen;
  color: #fff;
}

input[type=checkbox]:disabled + label:before {
  transform: scale(1);
  border-color: #aaa;
}

input[type=checkbox]:checked:disabled + label:before {
  transform: scale(1);
  background-color: #bfb;
  border-color: #bfb;
}

input[type="checkbox"] + label::before {
    content: "\2714";
    border: 0.1em solid #ccc;
    border-radius: 0.2em;
    display: inline-block;
    width: 2em;
    height: 1.5em;
    padding-left: 0.2em;
    padding-bottom: 0.3em;
    margin-right: 0.2em;
    vertical-align: bottom;
    color: transparent;
    transition: .2s;
}


     </style>

    @include('loader.css')
</head>

<body class="">

   
    @include('loader.html')
      
    <div class="wrapper " style="overflow: scroll;">
    
         @include($inc.'sidebar')
        
        <div class="main-panel">
           
            @include($inc.'nav')

            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                    <div id="app">
                        @yield('content')
                    </div>
                </div>
            </div>

          
    <!--   Core JS Files   -->
    <script src="{{URL('public/new-admin-master/assets/js/core/jquery.min.js')}}"></script>
    <script src="{{URL('public/new-admin-master/assets/js/core/popper.min.js')}}"></script>
    <script src="{{URL('public/new-admin-master/assets/js/core/bootstrap-material-design.min.js')}}"></script>
    <script src="{{URL('public/new-admin-master/assets/js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
    <!-- Plugin for the momentJs  -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/moment.min.js')}}"></script>
    <!--  Plugin for Sweet Alert -->
   
    <!-- Forms Validations Plugin -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/jquery.validate.min.js')}}"></script>
    <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/jquery.bootstrap-wizard.js')}}"></script>
    <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/bootstrap-selectpicker.js')}}"></script>
    <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/bootstrap-datetimepicker.min.js')}}"></script>
    <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/jquery.dataTables.min.js')}}"></script>
    <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/bootstrap-tagsinput.js')}}"></script>
    <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/jasny-bootstrap.min.js')}}"></script>
    <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/fullcalendar.min.js')}}"></script>
    <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/jquery-jvectormap.js')}}"></script>
    <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/nouislider.min.js')}}"></script>
    <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
    <script src="{{URL('https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js')}}"></script>
    <!-- Library for adding dinamically elements -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/arrive.min.js')}}"></script>
    <!--  Google Maps Plugin    -->
    <!-- Chartist JS -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/chartist.min.js')}}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{URL('public/new-admin-master/assets/js/plugins/bootstrap-notify.js')}}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{URL('public/new-admin-master/assets/js/material-dashboard.js')}}" type="text/javascript"></script>
    
    

    @include('loader.scripts')

    <link href="https://fonts.googleapis.com/css?family=Quicksand&display=swap" rel="stylesheet"> 
    <script src="{{URL('public/vue.js')}}"></script>
    @yield('scripts')
    <style>
     .quicksand{
        font-family: 'Quicksand', sans-serif;
     }
     #order_accepted{
        text-transform: uppercase;
        color: #070909;
        background: #77c323;
        font-weight: bold;
        text-align: center;
        letter-spacing: 1px;
        box-shadow: 2px 2px 2px rgb(206 205 205);
     }
    </style>
    <script>
        $(document).ready(function () {
            $().ready(function () {
                $sidebar = $('.sidebar');

                $sidebar_img_container = $sidebar.find('.sidebar-background');

                $full_page = $('.full-page');

                $sidebar_responsive = $('body > .navbar-collapse');

                window_width = $(window).width();

                fixed_plugin_open = $('.sidebar .sidebar-wrapper .nav li.active a p').html();

                if (window_width > 767 && fixed_plugin_open == 'Dashboard') {
                    if ($('.fixed-plugin .dropdown').hasClass('show-dropdown')) {
                        $('.fixed-plugin .dropdown').addClass('open');
                    }

                }

                $('.fixed-plugin a').click(function (event) {
                    // Alex if we click on switch, stop propagation of the event, so the dropdown will not be hide, otherwise we set the  section active
                    if ($(this).hasClass('switch-trigger')) {
                        if (event.stopPropagation) {
                            event.stopPropagation();
                        } else if (window.event) {
                            window.event.cancelBubble = true;
                        }
                    }
                });

                $('.fixed-plugin .active-color span').click(function () {
                    $full_page_background = $('.full-page-background');

                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-color', new_color);
                    }

                    if ($full_page.length != 0) {
                        $full_page.attr('filter-color', new_color);
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.attr('data-color', new_color);
                    }
                });

                $('.fixed-plugin .background-color .badge').click(function () {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');

                    var new_color = $(this).data('background-color');

                    if ($sidebar.length != 0) {
                        $sidebar.attr('data-background-color', new_color);
                    }
                });

                $('.fixed-plugin .img-holder').click(function () {
                    $full_page_background = $('.full-page-background');

                    $(this).parent('li').siblings().removeClass('active');
                    $(this).parent('li').addClass('active');


                    var new_image = $(this).find("img").attr('src');

                    if ($sidebar_img_container.length != 0 && $(
                            '.switch-sidebar-image input:checked').length != 0) {
                        $sidebar_img_container.fadeOut('fast', function () {
                            $sidebar_img_container.css('background-image', 'url("' +
                                new_image + '")');
                            $sidebar_img_container.fadeIn('fast');
                        });
                    }

                    if ($full_page_background.length != 0 && $(
                            '.switch-sidebar-image input:checked').length != 0) {
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                            'img').data('src');

                        $full_page_background.fadeOut('fast', function () {
                            $full_page_background.css('background-image', 'url("' +
                                new_image_full_page + '")');
                            $full_page_background.fadeIn('fast');
                        });
                    }

                    if ($('.switch-sidebar-image input:checked').length == 0) {
                        var new_image = $('.fixed-plugin li.active .img-holder').find("img")
                            .attr('src');
                        var new_image_full_page = $('.fixed-plugin li.active .img-holder').find(
                            'img').data('src');

                        $sidebar_img_container.css('background-image', 'url("' + new_image +
                            '")');
                        $full_page_background.css('background-image', 'url("' +
                            new_image_full_page + '")');
                    }

                    if ($sidebar_responsive.length != 0) {
                        $sidebar_responsive.css('background-image', 'url("' + new_image + '")');
                    }
                });

                $('.switch-sidebar-image input').change(function () {
                    $full_page_background = $('.full-page-background');

                    $input = $(this);

                    if ($input.is(':checked')) {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar_img_container.fadeIn('fast');
                            $sidebar.attr('data-image', '#');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page_background.fadeIn('fast');
                            $full_page.attr('data-image', '#');
                        }

                        background_image = true;
                    } else {
                        if ($sidebar_img_container.length != 0) {
                            $sidebar.removeAttr('data-image');
                            $sidebar_img_container.fadeOut('fast');
                        }

                        if ($full_page_background.length != 0) {
                            $full_page.removeAttr('data-image', '#');
                            $full_page_background.fadeOut('fast');
                        }

                        background_image = false;
                    }
                });

                $('.switch-sidebar-mini input').change(function () {
                    $body = $('body');

                    $input = $(this);

                    if (md.misc.sidebar_mini_active == true) {
                        $('body').removeClass('sidebar-mini');
                        md.misc.sidebar_mini_active = false;

                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar();

                    } else {

                        $('.sidebar .sidebar-wrapper, .main-panel').perfectScrollbar('destroy');

                        setTimeout(function () {
                            $('body').addClass('sidebar-mini');

                            md.misc.sidebar_mini_active = true;
                        }, 300);
                    }

                    // we simulate the window Resize so the charts will get updated in realtime.
                    var simulateWindowResize = setInterval(function () {
                        window.dispatchEvent(new Event('resize'));
                    }, 180);

                    // we stop the simulation of Window Resize after the animations are completed
                    setTimeout(function () {
                        clearInterval(simulateWindowResize);
                    }, 1000);

                });
            });
        });

    </script>
    <script>
        $(document).ready(function () {
            // Javascript method's body can be found in assets/js/demos.js')}}
            md.initDashboardPageCharts();

        });

    </script>

    <style>
    .form-group .bmd-label-static {
    top: -20px;
}

#loading {
		-webkit-animation: rotation 2s infinite linear;
}

@-webkit-keyframes rotation {
		from {
				-webkit-transform: rotate(0deg);
		}
		to {
				-webkit-transform: rotate(359deg);
		}
}

.f15{
    font-size: 15px;
}

.card .card-body .form-group {
    margin: 20px 0 20px;
}
label{
    color: #5A4D4D !important;
}

.page-item.active .page-link {
    z-index: 1;
    color: #ffffff;
    background-color: #a540b8;
    border-color: #a13ab6;
}

.card-profile .card-avatar {
            border-radius: 30px;
        }
        .card-profile .card-avatar {

         max-width: 200px !important;

        }



@media screen and (min-width:  481px) {

  .sticky {
  position: fixed;
  top:50px;
  width: 300px;
}

.sticky + .content {
  padding-top: 60px;
}

.mt-10{
    margin-top: 5rem;
}

    
}

input[type=checkbox] + label {
  display: inline;
  margin: 0.2em;
  cursor: pointer;
  padding: 0.2em;
}

.form-control{
    border-top: 1px solid #d4d4d4;
}

input[type=checkbox] {
  display: none;
}

input[type=checkbox] + label:before {
  content: "\2714";
  border: 0.1em solid #000;
  border-radius: 0.2em;
  display: inline-block;
  width: 1em;
  height: 1em;
  padding-left: 0.2em;
  padding-bottom: 0.3em;
  margin-right: 0.2em;
  vertical-align: bottom;
  color: transparent;
  transition: .2s;
}

input[type=checkbox] + label:active:before {
  transform: scale(0);
}

input[type=checkbox]:checked + label:before {
  background-color: MediumSeaGreen;
  border-color: MediumSeaGreen;
  color: #fff;
}

input[type=checkbox]:disabled + label:before {
  transform: scale(1);
  border-color: #aaa;
}

input[type=checkbox]:checked:disabled + label:before {
  transform: scale(1);
  background-color: #bfb;
  border-color: #bfb;
}

input[type="checkbox"] + label::before {
    content: "\2714";
    border: 0.1em solid #ccc;
    border-radius: 0.2em;
    display: inline-block;
    width: 2em;
    height: 1.5em;
    padding-left: 0.2em;
    padding-bottom: 0.3em;
    margin-right: 0.2em;
    vertical-align: bottom;
    color: transparent;
    transition: .2s;
}

.sidebar .sidebar-wrapper{
    overflow: scroll;
    -ms-overflow-style: none; 
    scrollbar-width: none;  
}
.sidebar-wrapper::-webkit-scrollbar{
    display: none; 
    overflow: scroll;
}

.sidebar {
overflow: scroll;
scroll-behavior: none;
scrollbar-width: none;
}

.bg-primary {
  background-color: #264a68 !important;
}

.card .card-header-success .card-icon, .card .card-header-success .card-text, .card .card-header-success:not(.card-header-icon):not(.card-header-text), .card.bg-success, .card.card-rotate.bg-success .front, .card.card-rotate.bg-success .back {
    background: linear-gradient(
2deg
, #070909, #43a047);
}
.tooltip-inner {background-color: black;color: white}

    </style>



<script>
        function filterCustomOptionSelect(){
            document.filter_form.date[6].selected = true
            document.filter_form.from.required = true
            document.filter_form.to.required = true
        }
        
        function selectDateOption(select){ 
            if( select.value  != "Custom") {
                document.filter_form.from.required = false
                document.filter_form.to.required = false
                document.filter_form.from.value = ""
                document.filter_form.to.value = ""
            }else {
                document.filter_form.from.required = true
            document.filter_form.to.required = true
            }
        }
        
        function imageUpload(id){
               const img = document.getElementById(id);
               const file = img.files[0];
               const url  = URL.createObjectURL(file);
               const parent = img.parentNode;
               parent.children[4].src= url;
        }
        

        function picUploadClick(id){
          document.getElementById(id).click()
        }
        
        $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
        
        </script>
         <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

        

@if(session()->has('success'))
<script>
Swal.fire({
  icon: 'success',
  title: 'Good Job!',
  text: '{!! session()->get('success') !!}'
})
</script>
@endif

@if(session()->has('unsuccess'))
<script>
    Swal.fire({       
      icon: 'error',
      title: 'Oops!',
      text: '{!! session()->get('unsuccess') !!}' ,
    })
    </script>
@endif

@if($errors->any())
<script>
    Swal.fire({       
      icon: 'error',
      title: 'Oops!',
      text: '{!! $errors->first() !!}' ,
    })
    </script>
@endif

@yield("important")
<style>

.bg-primary {
                background-color: #2b632e !important;
        }

</style>
</body>

</html>