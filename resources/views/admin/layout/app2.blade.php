<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Planet Vegan</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ADMIN_URL()}}/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="{{ADMIN_URL()}}/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ADMIN_URL()}}/assets/libs/css/style.css">
    <link rel="stylesheet" href="{{ADMIN_URL()}}/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <style>
       .btn-sm {
            padding: 2px 10px;
            font-size: 11px;
        }
    </style>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand" href="#">Planet Vegan</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
          
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        @include($inc."sidebar")
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                           @yield('content')
                    </div>
                </div>
            </div>
         
        </div>
        <!-- ============================================================== -->
        <!-- end main wrapper -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <script src="{{ADMIN_URL()}}/assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <script src="{{ADMIN_URL()}}/assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <script src="{{ADMIN_URL()}}/assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <script src="{{ADMIN_URL()}}/assets/libs/js/main-js.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @yield('scripts')
    
    @if(session()->has("success"))
    <script>
        swal({
                title: "Good Job!",
                text:  "{{session()->get('success')}}",
                icon: "success",
        });
    </script>
    @endif

    @if(session()->has("unsuccess"))
    <script>
        swal({
                title: "Oops!",
                text:  "{{session()->get('unsuccess')}}",
                icon: "warning",
        });
    </script>
    @endif

    @if($errors->any())
      <script>
         swal({
                title: "Oops!",
                text:  "{{$errors->first()}}",
                icon: "warning",
           });
      </script>
    @endif



</body>

</html>
