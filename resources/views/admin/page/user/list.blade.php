@extends($ADMIN_EXTEND)
@section('content')

<div class="card">

    <div class="card-header card-header-success">
        <h4 class="card-title "> <i class="fa fa-user-plus"></i> &nbsp Users List</h4>
        <p class="card-category">Latest users list from Planet Vegan APP</p>
        <div class="text-right">
        </div>
    </div>

    <div class="card-body">
        {{$list->links()}}
        <div class="table-responsive">
            <table class="table  table-striped table-bordered">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th>Address</th>
                    <th>Type</th>
                </tr>
                @foreach ($list as $item)
                <tr>
                    <td> {{$item->id}} </td>
                    <td> {{$item->name}} </td>
                    <td> {{$item->email}} </td>
                    <td> {{$item->mobile}} </td>
                    <td> {{$item->adr->first()->street_address ?? ""}} </td>
                    <td> {{$item->adr->first()->type ?? ""}} </td>
                </tr>
                @endforeach
            </table>
        </div>
        {{$list->links()}}
    </div>
</div>

@endsection
