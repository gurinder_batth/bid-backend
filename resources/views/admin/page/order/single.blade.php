@extends($ADMIN_EXTEND)
@section('content')

<div class="card">
    <div class="card-header card-header-success">
        <p style="float:right"> {{$order->created_at->format("M-d-y")}} {{$order->created_at->format("h:i:a")}} </p>
        <h4 class="card-title "> <i class="fa fa-shopping-cart"></i>

            <strong>${{$order->payment_recieved}} </strong>
        </h4>
        <p class="card-category"> <small class="text-info"> &nbsp #{{$order->id}} </small> {{$order->shipping_address}}
        </p>
        <div class="text-right">
        </div>
    </div>

    

    <div class="card-body">

     <div class="row mb-2">
          <div class="col-md-12">
            <button 
                 data-toggle="tooltip" data-placement="top" title="Press button to Accept the Order #{{$order->id}}"
                 class="btn btn-success btn-sm" >
              Accept Order <i class="fa fa-check"></i>
            </button>
            <button 
                 data-toggle="tooltip" data-placement="top" title="Press button to Download the Order #{{$order->id}}"
                 class="btn btn-primary btn-sm" >
              Download Invoice <i class="fa fa-print"></i>
            </button>
          </div>
     </div>

        <div class="table-responsive">
            <table class="table   table-striped table-bordered">
                <tr class="">
                    <th>Product</th>
                    <th>Qun</th>
                    <th>Price</th>
                    <th>Total</th>
                    <th>Review</th>
                </tr>
                @foreach ($order->cart as $item)
                <tr>
                    <td> {{$item->product_title}} </td>
                    <td> {{$item->qun}}X </td>
                    <td> ${{$item->single_price}} </td>
                    <td> ${{$item->total_price}} </td>
                    <td> {{$item->review}} </td>
                </tr>
                @endforeach
            </table>
        </div>

        <div class="containerid " style="margin-bottom:3rem">
          <section class="step-indicator {{$order->hasActiveStatus(1)}}">
              <div class="step step1  {{$order->hasActiveStatus(1)}}" >
                  <div class="step-icon">1</div>
                <p>Delivery</p>
              </div>
            <div class="indicator-line {{$order->hasActiveStatus(2)}}"></div>
            <div class="step step2 {{$order->hasActiveStatus(2)}}">
                <div class="step-icon">2</div>
              <p>Payment</p>
            </div>
            <div class="indicator-line {{$order->hasActiveStatus(3)}}"></div>
            <div class="step step3 {{$order->hasActiveStatus(3)}}">
                <div class="step-icon">3</div>
              <p>Accept</p>
            </div>
            <div class="indicator-line {{$order->hasActiveStatus(4)}}"></div>
            <div class="step step3 {{$order->hasActiveStatus(4)}}">
                <div class="step-icon">3</div>
              <p>Deliever</p>
            </div>
          </section>
        </div>
  

        <table class="table " align="right" style="width:300px">
            <tr>
                <th>Order Id</th>
                <th>#{{$order->id}}</th>
            </tr>
            <tr>
                <th>Cart Total</th>
                <th>${{$order->total_price}}</th>
            </tr>
            <tr>
                <th>Tax</th>
                <th>${{$order->tax}}</th>
            </tr>
            <tr>
                <th>Delievery</th>
                <th>${{$order->deliver_charge}}</th>
            </tr>
            <tr>
                <th>Received Amount</th>
                <th>${{$order->payment_recieved}}</th>
            </tr>

            <tr >
               <td colspan="2" class="text-right"> <strong>Payment Id:</strong> {{$order->payment_id}}</td>
           </tr>
           
        </table>
    </div>
</div>


@endsection


@section('css')
  <style>
   
   .containerid {
     max-width: 1200px;
     margin: 0 auto;
   }


     .step-indicator {
     margin-top: 50px;
     display: flex;
     align-items: center;
     padding: 0 40px;
     }

     .step {
     display: flex;
     align-items: center;
     flex-direction: column;
     position: relative;
     z-index: 1;
     }

     .step-indicator .step-icon {
     height: 50px;
     width: 50px;
     border-radius: 50%;
     background: #070909;;
     font-size: 10px;
     text-align: center;
     color: #ffffff;
     position: relative;
     line-height: 50px;
     font-size: 20px;
     }

     .step.active .step-icon {
          background: #77c323;
     }

     .step p {
     text-align: center;
     position: absolute;
     bottom: -40px;
     color: #070909;;
     font-size: 14px;
     font-weight: bold;
     }

     .step.active p {
          color: #77c323;
     }

     .step.step2 p,
     .step.step3 p {
     left: 50%;
     transform: translateX(-50%);
     }

     .indicator-line {
     width: 100%;
     height: 2px;
     background: #070909;;
     flex: 1;
     }

     .indicator-line.active {
     background: #77c323;
     }

     @media screen and (max-width: 500px) {
     .step p {
     font-size: 11px;
     bottom: -20px;
     }
     }
     .step-indicator .step-icon {
          height: 35px;
          width: 35px;
          border-radius: 50%;
          background: #070909;
          font-size: 10px;
          text-align: center;
          color: #ffffff;
          position: relative;
          line-height: 35px;
          font-size: 15px;
          }
</style>
@endsection