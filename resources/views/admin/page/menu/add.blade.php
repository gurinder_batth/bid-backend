@extends($ADMIN_EXTEND)
@section('content')

 <div class="card">
   <div class="card-header">
      <h4>Add Category</h4>
   </div>
    <div class="card-body">
         <form action="{{a_route('menu.create')}}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Category Title</label>
                       <input type="text" class="form-control" name="title" value="{{old("title")}}" required>
                   </div>
               </div>
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Category Description</label>
                       <input type="text" class="form-control" name="description" value="{{old("description")}}"  required>
                   </div>
               </div>
               <div class="col-md-12">
                   <div class="form-group">
                       <label for="">Category Image</label>
                       <input type="file" class="form-control" id="upload_img" name="temp_image" style="display:none" onchange="document.getElementById('productimg_file').innerHTML = this.files[0].name"  required>
                       <p id="productimg_file"></p>
                       <button type="button" class="btn btn-primary btn-sm" onclick="document.getElementById('upload_img').click()">Upload Image</button>
                   </div>
               </div>
               <div class="col-md-12 mt-2">
                   <div class="form-group text-left">
                       <button class="btn btn-primary">
                          Submit
                       </button>
                   </div>
               </div>
            </div>
         </form>
    </div>
 </div>

@endsection