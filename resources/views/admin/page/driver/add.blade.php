@extends($ADMIN_EXTEND)
@section('content')

 <div class="card">

    <div class="card-body">
         <form action="{{a_route('driver.create')}}" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
            <div class="row">

                <div class="col-md-6">
                    <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-user"></i> <strong class="text-danger">*</strong>
                            </span>
                            </div>
                        <input type="text" class="form-control" placeholder="NAME" required name="name" value="{{old('name')}}"  aria-describedby="basic-addon1">
                    </div>
               </div>

               <div class="col-md-12"></div>

               <div class="col-md-6">
                    <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-at"></i> <strong class="text-danger">*</strong>
                            </span>
                            </div>
                        <input type="email" class="form-control" required name="email" value="{{old('email')}}"  placeholder="EMAIL ADDRESS"  aria-describedby="basic-addon1">
                    </div>
               </div>

         

               <div class="col-md-6">
                    <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-phone"></i> <strong class="text-danger">*</strong>
                            </span>
                            </div>
                        <input type="text" class="form-control" required placeholder="MOBILE" name="mobile" value="{{old('mobile')}}"   aria-describedby="basic-addon1">
                    </div>
               </div>

               <div class="col-md-12"></div>

              

               <div class="col-md-6">
                    <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa fa-motorcycle"></i>
                            </span>
                            </div>
                        <input type="text" class="form-control"  placeholder="VEHICLE NAME" name="vehicle_name" value="{{old('vehicle_name')}}"   aria-describedby="basic-addon1">
                    </div>
               </div>

               <div class="col-md-6">
                    <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa  fa-sort-numeric-desc"></i> 
                            </span>
                            </div>
                        <input type="text" class="form-control"  placeholder="VEHICLE NUMBER" name="vehicle_number" value="{{old('vehicle_number')}}"  aria-describedby="basic-addon1">
                    </div>
               </div>

               <div class="col-md-6">
                    <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa  fa-lock"></i> 
                            </span>
                            </div>
                        <input type="password" class="form-control" required placeholder="PASSWORD" name="password" aria-describedby="basic-addon1">
                    </div>
               </div>

               <div class="col-md-6">
                    <div class="input-group mb-3">
                            <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <i class="fa  fa-lock"></i> 
                            </span>
                            </div>
                        <input type="password" class="form-control" required name="password_confirmation" placeholder="CONFIRM PASSWORD"  aria-describedby="basic-addon1">
                    </div>
               </div>

       
               <div class="col-md-12 ">
                   <div class="form-group text-left">
                       <button class="btn btn-primary btn-sm btn-pv" style="margin-left:2rem">
                         CREATE DRIVER
                       </button>
                   </div>
               </div>
            </div>
         </form>
    </div>
 </div>

@endsection


@section('scripts')
  @include($inc."nq_script")
@endsection