@extends($ADMIN_EXTEND)
@section('content')

<div class="">
    <div class="">
        <div class="row">



            <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="card card-stats">
                      <div class="card-header card-header-danger card-header-icon">
                          <div class="card-icon">
                              <i class="material-icons">shopping_cart     </i>
                          </div>
                          <p class="card-category">NEW ORDER</p>
                          <h3 class="card-title">{{$neworders}}
                          </h3>
                      </div>
                      <div class="card-footer">
                      </div>
                  </div>
              </div>
  
              <div class="col-lg-4 col-md-6 col-sm-6">
                  <div class="card card-stats">
                      <div class="card-header card-header-warning card-header-icon">
                          <div class="card-icon">
                              <i class="material-icons">two_wheeler</i>
                          </div>
                          <p class="card-category">ON THE WAY</p>
                          <h3 class="card-title">{{$ontheway}}
                          </h3>
                      </div>
                      <div class="card-footer">
                      </div>
                  </div>
              </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-success card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">phone</i>
                        </div>
                        <p class="card-category">PRODUCT</p>
                        <h3 class="card-title">{{$product}}
                        </h3>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-primary card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">list</i>
                        </div>
                        <p class="card-category">MENU</p>
                        <h3 class="card-title">{{$menu}}
                        </h3>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-stats">
                    <div class="card-header card-header-info card-header-icon">
                        <div class="card-icon">
                            <i class="material-icons">person</i>
                        </div>
                        <p class="card-category">CLIENTS</p>
                        <h3 class="card-title">{{$users}}
                        </h3>
                    </div>
                    <div class="card-footer">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
