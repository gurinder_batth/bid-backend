<!-- Modal -->
<div class="modal fade" id="accept_order"  tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Assign <b><span id="order_id"></span> Order</b> </h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="card">
                <div class="card-body" id="order_content">
                </div>
            </div>
            <form action="{{a_route('order.accept')}}" method="post" name="order_accept_form">
              {{ csrf_field() }}
               <input type="hidden" name="id" id="order_id_db">
                <div class="form-group">
                      <select name="driver_id" id="" class="form-control" required>
                          <option value="">Select Driver *</option>
                           @foreach ($drivers as $item)
                              <option value="{{$item->id}}"> {{$item->name}} </option>
                           @endforeach
                      </select>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" onclick="document.order_accept_form.submit()">Save changes</button>
        </div>
      </div>
    </div>
  </div>