<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
               
                    @php
                        $admin_links = [
                               [
                                   "name" => "Dashboard" ,
                                   "icon" => "fa fa-fw fa-home",
                                   "menus" => [
                                      "Dashboard" => a_route("dashboard")
                                   ]
                               ] ,
                               [
                                   "name" => "Product" ,
                                   "icon" => "fa fa-fw fa-shopping-cart",
                                   "menus" => [
                                      "Add" => a_route("product.add") ,
                                      "List" => a_route("product.list") ,
                                   ]
                               ]  ,
                               [
                                   "name" => "Menu" ,
                                   "icon" => "fa fa-fw fa-bars",
                                   "menus" => [
                                      "Add" => a_route("menu.add") ,
                                      "List" => a_route("menu.list") ,
                                   ]
                               ]  ,
                               [
                                   "name" => "User" ,
                                   "icon" => "fa fa-fw fa-user",
                                   "menus" => [
                                      "List" => a_route("user.list") ,
                                   ]
                               ]  ,
                               [
                                   "name" => "Order" ,
                                   "icon" => "fa fa-fw fa-shopping-basket",
                                   "menus" => [
                                      "List" => a_route("order.list") ,
                                   ]
                               ]  ,
                         ];
                    @endphp

                     @foreach($admin_links as $i => $item)
                        <li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                data-target="#submenu-{{$i}}" aria-controls="submenu-{{$i}}"><i class="{{$item['icon']}}"></i>
                                  {{$item['name']}}
                            </a>
                            <div id="submenu-{{$i}}" class="collapse submenu" style="">
                                <ul class="nav flex-column">
                                    @foreach ($item['menus'] as $name => $route )
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{$route}}"> {{ $name }} </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                     @endforeach
                   


                </ul>
            </div>
        </nav>
    </div>
</div>
