-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 23, 2021 at 05:41 PM
-- Server version: 5.7.33-0ubuntu0.16.04.1
-- PHP Version: 7.3.27-9+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bidding`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_processings`
--

CREATE TABLE `cart_processings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `total_price` bigint(20) UNSIGNED NOT NULL,
  `qun` int(11) NOT NULL,
  `single_price` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rating` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vehicle_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fcm_driver_tokens`
--

CREATE TABLE `fcm_driver_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `driver_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fcm_user_tokens`
--

CREATE TABLE `fcm_user_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `description`, `image`, `trash`, `created_at`, `updated_at`) VALUES
(13, 'Earn Money', 'Earn Money', 'menu_images/1626680816/0ezUOiZkNMK16W14QgQdRiSBihfD4Her46FgJE7D.png', 0, '2021-07-19 02:16:56', '2021-07-19 02:16:56');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_27_110018_add_fb_id_column_in_users_table', 1),
(5, '2021_04_16_122730_user_fields', 1),
(6, '2021_04_23_055931_create_products_table', 2),
(7, '2021_04_23_061102_create_menus_table', 2),
(8, '2021_04_30_044527_create_cart_processings_table', 3),
(9, '2021_04_30_064114_create_orders_table', 3),
(10, '2021_04_30_064504_orders_constrait', 3),
(11, '2021_05_10_135149_create_drivers_table', 4),
(12, '2021_05_11_060247_create_fcm_user_tokens_table', 4),
(13, '2021_05_11_060618_create_user_addresses_table', 4),
(14, '2021_05_11_065147_forcon', 4),
(15, '2021_05_14_133346_order_shipping_col', 4),
(16, '2021_05_14_135348_order_comment', 4),
(17, '2021_05_14_151130_trash_col_driver_and_user', 4),
(18, '2021_05_14_153240_order_status', 4),
(19, '2021_05_14_164444_order_keys_driver', 4),
(20, '2021_05_15_120329_create_fcm_driver_tokens_table', 4),
(21, '2021_05_22_102358_otpforuser', 4),
(22, '2021_05_23_142115_update_driver_location_order', 4),
(23, '2021_07_19_040956_productcols', 5),
(24, '2021_07_19_050621_order_del', 6),
(25, '2021_07_19_051058_order_add_product_id', 7),
(26, '2021_07_19_073527_productprice', 8);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `total_price` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `payment_recieved` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `number` double NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `total_price`, `user_id`, `payment_recieved`, `payment_id`, `created_at`, `updated_at`, `status`, `number`, `product_id`) VALUES
(1, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 01:40:44', '2021-07-23 01:40:44', 'pending', 10.64, 28),
(2, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 01:40:44', '2021-07-23 01:40:44', 'pending', 10.64, 28),
(3, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 01:40:46', '2021-07-23 01:40:46', 'pending', 10.64, 28),
(4, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:37', '2021-07-23 02:08:37', 'pending', 98, 28),
(5, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:37', '2021-07-23 02:08:37', 'pending', 98, 28),
(6, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:38', '2021-07-23 02:08:38', 'pending', 98, 28),
(7, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:08:38', '2021-07-23 02:08:38', 'pending', 98, 28),
(8, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:10:32', '2021-07-23 02:10:32', 'pending', 895, 28),
(9, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:12:10', '2021-07-23 02:12:10', 'pending', 10, 28),
(10, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:36:14', '2021-07-23 02:36:14', 'pending', 89, 28),
(11, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:37:03', '2021-07-23 02:37:03', 'pending', 12.6, 28),
(12, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:41:19', '2021-07-23 02:41:19', 'pending', 10.64, 28),
(13, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:44:32', '2021-07-23 02:44:32', 'pending', 16.646, 28),
(14, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:45:21', '2021-07-23 02:45:21', 'pending', 10.25, 28),
(15, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:45:38', '2021-07-23 02:45:38', 'pending', 10.25, 28),
(16, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:45:56', '2021-07-23 02:45:56', 'pending', 10.63, 28),
(17, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:46:08', '2021-07-23 02:46:08', 'pending', 10.94, 28),
(18, 10, 13, 0, 'nwiji786ryh8467974w9', '2021-07-23 02:46:28', '2021-07-23 02:46:28', 'pending', 10.64, 28);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `popular` tinyint(1) NOT NULL DEFAULT '0',
  `menu_id` bigint(20) UNSIGNED DEFAULT NULL,
  `nutrition_qualities` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `start_from` datetime NOT NULL,
  `start_to` datetime NOT NULL,
  `bidprice` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `image`, `trash`, `popular`, `menu_id`, `nutrition_qualities`, `created_at`, `updated_at`, `start_from`, `start_to`, `bidprice`) VALUES
(28, 'Win Samsung LED', 'Win brand new led tv of 15000', 10, 'menu_images/1627028446/FUHoydMxHMzKUY5cJR7yX0S24KB9W7VeOGUj7yeP.jpg', 0, 0, 13, NULL, '2021-07-19 02:16:36', '2021-07-23 02:50:46', '2021-07-23 12:00:00', '2021-07-23 12:00:00', 10),
(29, 'Brand New Sony LED', 'Brand New Sony LED', 30000, 'menu_images/1627028531/Xo0OvPsdp3mISwLXnIElPHiU8nbmh9TJRvnUVeAD.jpg', 0, 0, 13, NULL, '2021-07-23 02:52:11', '2021-07-23 02:52:11', '2021-07-23 02:00:00', '2021-07-24 02:00:00', 10);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fb_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trash` tinyint(1) NOT NULL DEFAULT '0',
  `otp` int(11) DEFAULT NULL,
  `otp_attempt` int(11) NOT NULL DEFAULT '0',
  `otp_expire` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `fb_id`, `address`, `mobile`, `verify`, `token`, `trash`, `otp`, `otp_attempt`, `otp_expire`) VALUES
(1, 'Gurinder', 'gurinder@domainandspace.com', NULL, '123456', NULL, '2021-04-17 06:01:54', '2021-04-17 06:01:54', NULL, 'Sanrgrur', '9646848434', 0, NULL, 0, NULL, 0, NULL),
(3, 'Gurinder', 'gurinder@domainandspace.com1', NULL, '123456780', NULL, '2021-04-17 06:09:39', '2021-04-17 06:09:39', NULL, 'Sanrgrur', '96468484340', 0, NULL, 0, NULL, 0, NULL),
(4, 'Gurinder', 'gurinder@domainandspace1.com1', NULL, '123456780', NULL, '2021-04-17 06:10:24', '2021-04-17 06:10:24', NULL, 'Sanrgrur1', '9646848411340', 0, 'iJtiNlAvHWpUEd0k0z4hUdkP4KZzihdbWqY8cCxsBx1NH5Rt4nnyXgjfrByb', 0, NULL, 0, NULL),
(5, 'Gurinder', 'gurinder@domainandspace1.c', NULL, '$2y$10$s99wcJ.k8zysftf4woG.BeVEvYvQ0.nALY/oCDy415PrO17oIAcYW', NULL, '2021-04-17 06:31:00', '2021-04-17 06:31:00', NULL, 'Sanrgrur1', '96468484113', 0, 'D7Z0I93omMEwBPqPuZcqJSgzxBSoY5lsGKf5gtL4oJJfuGg0gbQwKeyQ8Wmw', 0, NULL, 0, NULL),
(6, 'Gurinder', 'gurinder1@domainandspace.com', NULL, '$2y$10$/MxNqT1QXXWavZZQLz0PC.4EnDhbKcxBTxJ4KunWBAFZxtT5dwNzq', NULL, '2021-04-21 05:47:02', '2021-04-21 05:47:02', NULL, 'Sangrur', '1234567890', 0, 'Sw7IFyb7K3O3xbghLztUBtLONVZxf4VgDqQEPu1sdsfJyMDsqEc8cDqWyLPs', 0, NULL, 0, NULL),
(7, 'Gurinder', 'gurinder12@domainandspace.com', NULL, '$2y$10$3inZZiaEwj1yVBRstBvihupMV.k1m3ff0PrKVSjajOL/oKBHjk.3m', NULL, '2021-04-21 06:07:49', '2021-04-21 06:07:50', NULL, 'Sangrur', '1234567891', 0, '4fEghX3Ac8MZI3860DszgaeaU3Qox9SQubRLiQB2JdSvjPbDm0I1ONc47INm', 0, NULL, 0, NULL),
(8, 'Gurinder', 'gurinderbatth08@gmail.com', NULL, '$2y$10$iacZJmqpFaXtSXrJjVKD3.aliAuVy8Q0KOWaIMMTRZpuMslVKhL/C', NULL, '2021-04-21 06:45:48', '2021-04-21 06:45:49', NULL, 'Sangrur', '9646848431', 0, 'oZvyI97kJ9FmG9Hbo9WKuWTO9UAqVHhEXKHIODokz4XZ9zrSFo0YD2OzPVNh', 0, NULL, 0, NULL),
(9, 'Gurinder', 'Gurinderbatth228@gmail.com', NULL, '$2y$10$Eept9cdc/aEjURjXFqPXReI9/Z94.HY0yojszUsoSGgPhXfmxCcbi', NULL, '2021-04-21 06:49:04', '2021-04-21 06:49:04', NULL, 'Sangrur', '9646848419', 0, 'B3wpOLP9V80ADK5CDEQzHOnlb3IGCUlbwN9929wB7v9NGPEcWUsPOxvmblg8', 0, NULL, 0, NULL),
(10, 'Gurinder', 'gurinderbah08@gmail.com', NULL, '$2y$10$akAzhx.iXktTBWAETkqd2OJDs1gW8vhRz19yCozGLBTXkM72aozdG', NULL, '2021-04-21 06:51:44', '2021-04-21 06:51:44', NULL, 'Sangrur', '9646848492', 0, '2nANL3JeH2XDCDZLuj7STYW7UCaCXN1ncQmg5msLRpmCFZf5QvxZoBIOb3nL', 0, NULL, 0, NULL),
(11, 'Gurinder', 'test@test.com', NULL, '$2y$10$oj3cDWjRcTbQyaLqGOb0je9Eo1k9bW0Cev4Po8lub7GSUyQoSCsF6', NULL, '2021-04-22 06:25:37', '2021-04-22 06:25:38', NULL, 'Sangrur', '12345678', 0, '5tYVjMR7ULcMmuikOp6hbL0HNdHIEAGfHTWQjz96U7L3ZunetIRKSpqn7QPz', 0, NULL, 0, NULL),
(12, 'Gurinder', 'user@user.com', NULL, '$2y$10$1pQ.vHVpa1kTSmgUZcwraOA33uL147yUqtEwt5VtVDgFlWvXg6RUm', NULL, '2021-04-22 06:28:04', '2021-04-22 06:28:04', NULL, 'Sangrur', '1234512345', 0, 'O9L3Mxc3fcgl25qFQHrjMFyU8q8AXc0akHYVatVIDUBRIHKo9HDSfLAxLrOV', 0, NULL, 0, NULL),
(13, 'Gurinder', 'Gurinderbatth@gmail.com', NULL, '$2y$10$zsfxMP0G4h5dLKhiTHE3BuJZb24GELGxmMvz8uAyZZF1NmWfA74nW', NULL, '2021-07-19 06:00:26', '2021-07-19 06:00:26', NULL, 'Sangrur', '9530652160', 0, 'vWJKOPxGcdPgoeKF3n19m5wKShZMb8kqbidUlnwqE8MX3B1eDLlT3ciKcWVa', 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `street_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_processings`
--
ALTER TABLE `cart_processings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_processings_order_id_foreign` (`order_id`),
  ADD KEY `cart_processings_user_id_foreign` (`user_id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fcm_driver_tokens`
--
ALTER TABLE `fcm_driver_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fcm_driver_tokens_driver_id_foreign` (`driver_id`);

--
-- Indexes for table `fcm_user_tokens`
--
ALTER TABLE `fcm_user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fcm_user_tokens_user_id_foreign` (`user_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_processings`
--
ALTER TABLE `cart_processings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fcm_driver_tokens`
--
ALTER TABLE `fcm_driver_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fcm_user_tokens`
--
ALTER TABLE `fcm_user_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_processings`
--
ALTER TABLE `cart_processings`
  ADD CONSTRAINT `cart_processings_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_processings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fcm_driver_tokens`
--
ALTER TABLE `fcm_driver_tokens`
  ADD CONSTRAINT `fcm_driver_tokens_driver_id_foreign` FOREIGN KEY (`driver_id`) REFERENCES `drivers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `fcm_user_tokens`
--
ALTER TABLE `fcm_user_tokens`
  ADD CONSTRAINT `fcm_user_tokens_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
