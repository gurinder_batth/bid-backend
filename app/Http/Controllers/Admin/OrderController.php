<?php

namespace App\Http\Controllers\Admin;

use App\Models\Driver;
use App\Models\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Admin\Filter\OrderFilter;
use PDF;

class OrderController extends Controller
{
    public function list(Request $req,OrderFilter $filter){
        $filter->statusFilter()->searchFilter();
        $order = $filter->applyFilter();
        $orders = $order->latest()->paginate(10);
        $drivers = Driver::whereTrash(0)->latest()->get();
        return ADMIN_VIEW("order.list")->with([
            "orders" => $orders ,
            "drivers" => $drivers
        ]);
    }

    public function single(Request $req){
        $order = Order::find($req->id);
        return ADMIN_VIEW("order.single")->with([
            "order" => $order
        ]);
    }

    public function accept(Request $req){
        $req->validate([
            "id" => "required",
            "driver_id" => "required",
        ]);
        $order = Order::find($req->id);
        $driver = Driver::find($req->driver_id);
        $order->setOrderAccept($driver);
        $order->save();
        $order->setOrderAcceptNotification();
        $order->setOrderAcceptNotificationDriver();
        session()->flash("succcess","Order successfully Accepeted.");
        return redirect()->back();
    }

    public function inovice(Request $req) {
        $order = Order::find($req->id);
        $data = [
            'order' => $order ,
            'user' => $order->user ,
            'address' => $order->user->adr->first() ,
        ];
             
        $dompdf = PDF::loadView("inovice.index",$data);
        return $dompdf->stream();
    }

}
