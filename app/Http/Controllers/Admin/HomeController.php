<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use App\Models\User;
use App\Models\Product;
use App\Models\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
     public function dashboard(Request $req)    
     {
         $menu = Menu::whereTrash(0)->count();
         $product = Product::whereTrash(0)->count();
         $users = User::count();
         $neworders = Order::whereStatus("paid")->count();
         $ontheway = Order::whereStatus("accept")->count();
         return ADMIN_VIEW("dashboard")->with([
             'menu' => $menu , 
             'product' => $product ,
             'users' => $users ,
             'neworders' => $neworders ,
             'ontheway' => $ontheway ,
         ]);
     }
}
