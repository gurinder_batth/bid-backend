<?php

namespace App\Http\Controllers\Admin;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class MenuController extends Controller
{
    public function add(Request $req) {
        return ADMIN_VIEW("menu.add");
    }
    public function create(Request $req) {
            $req->validate([
               "title" => "required|max:255",
               "description" => "required|max:65555",
               "temp_image" => "required|file",
            ]);
            $menu = Menu::create($req->all());
            $name  = $req->temp_image->store("menu_images/".time());
            $menu->image =  $name;
            $menu->save();
            return redirect()->back();
    }

    public function list(Request $req)
    {
        $list = Menu::whereTrash(0)->latest()->paginate(20);
        return ADMIN_VIEW("menu.list", ['list' => $list ]);
    }

    public function delete(Request $req){
        $menu = Menu::find($req->id);
        $menu->trash = $req->flag;
        $menu->save();
        return redirect()->back();
    }
    public function edit(Request $req){
        $menu = Menu::find($req->id);
        return ADMIN_VIEW("menu.edit" ,["menu" => $menu]);
    }
    public function update(Request $req){
        $req->validate([
            "title" => "required|max:255",
            "description" => "required|max:65555"
         ]);
        $menu = Menu::find($req->id);
        $menu->update($req->all());

        if($req->temp_image){
            $file = $req->file('temp_image');
            $name = "menu_images/".time() . $file->getClientOriginalName();
            
            $status =  Storage::disk('s3')->put(
                        $name ,
                        file_get_contents($file->getRealPath()) ,
            );
            $menu->image =  $name;
            $menu->save();
        }

        return redirect()->back();
    }
}
