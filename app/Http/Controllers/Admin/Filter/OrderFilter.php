<?php

namespace App\Http\Controllers\Admin\Filter;

use App\Models\Model\Order;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;


class OrderFilter
{
     private $req;
     private $order;
     public function __construct(Request $req,Order $order)
     {
          $this->req = $req;
          $this->order = $order;
     }

     public function statusFilter(){
            if($this->req->status == "all" or $this->req->status == null){
                $this->order = $this->order;
                return $this;
            }
            $status = $this->req->status;
            $this->order = $this->order->whereStatus($status);
            return $this;
     }

     public function searchFilter(){
            if($this->req->str == null){
                $this->order = $this->order;
                return ;
            }
             $this->order = $this->order->whereHas('cart', function (Builder $query) {
                              $query->where('product_title',"LIKE","%".$this->req->str."%");
             });
             return $this;
     }

     public function applyFilter(){
         return $this->order;
     }
}
