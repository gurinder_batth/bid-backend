<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function list(Request $req)
    {
        $list = User::latest()->paginate(10);
        return ADMIN_VIEW("user.list", ['list' => $list ]);
    }

}
