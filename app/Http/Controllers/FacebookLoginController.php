<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class FacebookLoginController extends Controller
{
    public function index(Request $req)
    {
        return Socialite::driver('facebook')->redirect();
    }
    public function setup(Request $req)
    {
        return view("facebook.setup");
    }
    public function callback(Request $req)
    {

        try {
    
            $user = Socialite::driver('facebook')->user();
            dd($user);
            $isUser = User::where('fb_id', $user->id)->first();
     
            if($isUser){
                dd($isUser);
            }else{
                $createUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'fb_id' => $user->id,
                    'password' => encrypt('admin@123')
                ]);
                dd($createUser);
            }
    
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    public function delete(Request $req)
    {
        # code...
    }
}
