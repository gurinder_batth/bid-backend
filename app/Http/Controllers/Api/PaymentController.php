<?php

namespace App\Http\Controllers\Api;

use Exception;
use Stripe\Charge;
use Stripe\Stripe;
use App\Models\User;
use Stripe\Customer;
use App\Models\Model\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{

    public function charge(Request $req)
    {

        $error = $this->chargeValidation($req);
        if($error) { return $error; }
        
        $order = Order::find($req->order_id);

        try{
            Stripe::setApiKey(env('STRIPE_SECRET'));

            $user = User::find($req->user_id);

           $c =  Customer::create(array(
                'name' => $user->name ,
                'description' =>  "Order Id -> ". $order->id ,
                'email' => $user->email ,
                'source' =>  $req->token ,
                "address" => ["city" =>  $order->shipping_address , "country" => "US", "line1" => $order->shipping_address , "line2" => "", "postal_code" => $user->shipping_address , "state" => $user->shipping_address ]
           ));

            // return response()->json(['status' => false , 'customer' => $c , 'c' => $c->id  ]); 
            $pay = ($order->total_price + $order->tax + $order->deliver_charge) * 100;
            $charge =  Charge::create ([
                        "amount" => $pay,
                        "currency" => "usd",
                        "customer" => $c->id ,
                        "source" => $c->default_source ,
                        "description" =>  "Order Id -> ". $order->id ,
            ]);
        }
        catch(Exception $e){
            return response()->json(['status' => false , 'errors' => [$e->getMessage()]  ]);  
        }

        if($charge->paid){
            $order->payment_id = $charge->id;
            $order->payment_recieved = $charge->amount / 100;
            $order->setOrderPaid();
            $order->save();
        }

        return response()->json(['charge' => $charge , 'status' => true , 'pay' =>  $pay , "total_price" => $order->total_price , "tax" =>  $order->tax ]);  
    }

    public function chargeValidation(Request $req)
    {
          $validator = Validator::make($req->all(),[
                    "token" => "required|min:3",
                    "user_id" => "required|exists:users,id",
                    "order_id" => "required|exists:orders,id",
          ]);

          if( $validator->fails() ){
              return response()->json([
                  "status" => false  ,
                  "errors" => $validator->errors()->all() ,
                  "message" => "Oops! invalid data"
              ]);
          }
          return false;
    }
}
