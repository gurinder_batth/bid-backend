<?php

namespace App\Models;

use App\Models\Menu;
use App\Models\Model\Order;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        "title" , "description" , "price", "image" , "menu_id" , "trash" , "popular" ,
        "start_from" , "start_to" , "bidprice"
    ];
    protected $dates = [
        "start_from" , "start_to"
    ];
    protected $hidden = [ "nutrition" ,"nutrition_qualities" ];
    protected $appends = [
        "img_path" ,  "start_date" , "expire_date"
    ];

    public function getStartDateAttribute()
    {
        return $this->start_from->format("h:iA d-M-y");
    }
    public function getExpireDateAttribute()
    {
        return $this->start_to->format("h:iA d-M-y");
    }

    public function updateNutritionQualities(Request $req){
        $data = [];
        for($i = 0 ; $i < count($req->n_number) ; $i++){
             array_push($data,[
                 "number" => $req->n_number[$i] ,
                 "name" => $req->n_name[$i] ,
                 "percentange" => $req->n_percentange[$i] ,
             ]);
        }
        $this->nutrition_qualities = json_encode($data);
        return $data;
    }

    public function getNQ(){
           if($this->nutrition_qualities == null){
               return [];
           }
           return json_decode($this->nutrition_qualities);
    }

    public function menu(){
       return $this->hasOne(Menu::class,"id","menu_id");
    }

    public function getImgPathAttribute(){
        return AWS_URL($this->image);
    }

    public function getNutritionAttribute(){
        return $this->getNQ();
    }

    public function speficUserOrder()
    {
        return $this->hasMany(Order::class)->whereUserId(request()->user_id);
    }

}
