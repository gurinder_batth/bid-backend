<?php

namespace App\Models;

use App\Models\UserAddress;
use App\Models\FcmUserToken;
use App\Models\Helper\OtpHelper;
use App\Models\Helper\OtpInterface;
use Illuminate\Notifications\Notifiable;
use App\PushNotification\PushNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements OtpInterface
{
    use HasFactory, Notifiable , PushNotification , OtpHelper;

    protected $appends = ['type_user'];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'address',
        'password',
        'fb_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function adr(){
      return $this->hasMany(UserAddress::class,'user_id','id');
    }

    public function getTypeUserAttribute($val){
        return "user";
    }

    public function fcmtoken(){
        return $this->hasOne(FcmUserToken::class,'user_id','id');
    }
}
