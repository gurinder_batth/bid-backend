<?php

namespace App\Models\Model;

use App\Models\Product;
use App\Models\Model\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CartProcessing extends Model
{
    use HasFactory;

    protected $fillable = [

         "order_id" , "total_price",   "single_price" , "qun" ,  "product_id" ,  "product_title" , 
          "review" ,   "rating" ,     "user_id" 
           
    ];

    protected $appends = [ "order_date" ];

    public function order()
    {
        return $this->hasOne(Order::class,'id','order_id');
    }

    public function getOrderDateAttribute($val)
    {
        return $this->created_at->format("M d,Y");
    }
    
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
}
