<?php

namespace App\Models;

use App\Models\FcmUserToken;
use Illuminate\Database\Eloquent\Model;
use App\PushNotification\PushNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Driver extends Authenticatable
{
    use HasFactory ,PushNotification;

    protected $appends = ['type_user'];

    protected $fillable = [
        'name',
        'email',
        'mobile' ,
        'vehicle_name',
        'vehicle_number',
        'password',
    ]; 

    public function getTypeUserAttribute($val){
        return "driver";
    }


    public function fcmtoken(){
        return $this->hasOne(FcmDriverToken::class,'driver_id','id');
    }
}
