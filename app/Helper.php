<?php

function ADMIN_VIEW($name,$data = []){
    return view("admin.page.".$name)->with($data)
            ->with([
                'ADMIN_EXTEND' => 'admin.layout.app',
                'inc' => 'admin.inc.',
            ]);
}

function ADMIN_URL($url = null){
  return URL("public/admin" . $url);
}

function a_route($name,$data = []){
  return route("admin.".$name , $data);
}

function AWS_URL($name){
  // return env("AWS_URL")."/".$name;
  return URL("storage/app")."/".$name;
}
