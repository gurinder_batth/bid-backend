<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedDouble("total_price");
            $table->unsignedDouble("tax");
            $table->unsignedDouble("deliver_charge");
            $table->dateTime("deliver_at")->nullable();
            $table->unsignedBigInteger("user_id");
            $table->unsignedDouble("payment_recieved")->default(0);
            $table->string("payment_id")->null();
            $table->boolean("status")->default(1);
            $table->string("shipping_address");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
