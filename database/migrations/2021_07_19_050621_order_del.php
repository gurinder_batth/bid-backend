<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderDel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
                $table->dropColumn([
                   'deliver_charge', 'deliver_at', 'tax' , 
                   'shipping_address' ,'city' ,'zip_code',
                   'state','latitude','longitude','type',
                   'comments','driver_id','cancel_reason'
                ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
